# marktask

`marktask` is a python module to mark files and list or folders with key-value 
relations. The task of this module is to provide a python and command line interface
for running job like processes in python, which bases on multiple processing steps
on and around files.

## Installing

Installing the latest release using pip is recommended.

````shell script
$ pip install marktask
````

The latest development state can be obtained from gitlab using pip.

````shell script
$ pip install git+https://gitlab.com/david.scheliga/marktask.git@dev
````

## Authors

* **David Scheliga** 
    [@gitlab](https://gitlab.com/david.scheliga)
    [@Linkedin](https://www.linkedin.com/in/david-scheliga-576984171/)
    - Initial work
    - Maintainer

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE - see the
[LICENSE](https://gitlab.com/david.scheliga/dicthandling/blob/master/LICENSE) file for details

## Acknowledge

- [Code style: black](https://github.com/psf/black)
- [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
- [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
