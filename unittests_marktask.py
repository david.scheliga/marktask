import shutil
import time
import unittest
import tempfile
from pathlib import Path
import myminions
import marktask
from marktask import Task, state_of_tasks_are


def list_marker_files(root_path: Path):
    return list(root_path.rglob("*" + marktask.DEFAULT_TASK_FILE_NAME))


def remove_marker_files(root_path: Path):
    for filepath in list_marker_files(root_path):
        filepath.unlink()


class TestCommands(unittest.TestCase):
    TEST_FOLDER_PATHS = ["folder-1", "folder-2", "folder-2/folder-3"]
    TEST_FILE_PATHS = ["folder-1/file-1.txt", "folder-2/folder-3/file-3.txt"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._temporary_dir = None
        self._temporary_test_paths = []

    def setUp(self) -> None:
        self._temporary_dir = Path(tempfile.mkdtemp())
        for test_path in self.TEST_FOLDER_PATHS:
            test_path = self._temporary_dir.joinpath(test_path)
            test_path.mkdir()
            self._temporary_test_paths.append(test_path)
        for test_filepath in self.TEST_FILE_PATHS:
            test_filepath = self._temporary_dir.joinpath(test_filepath)
            test_filepath.touch()
            self._temporary_test_paths.append(test_filepath)

    def test_mark_with_many_paths(self):
        remove_marker_files(self._temporary_dir)
        paths_to_mark = [str(path) for path in self._temporary_test_paths]
        cli_arguments = [
            marktask.COMMAND_MARK_SHORT,
            "marker:test,test:done",
            *[str(path) for path in self._temporary_test_paths],
        ]
        marktask.main(cli_arguments)
        marking_file_paths_which_should_exist = [
            marktask.propose_task_filepath(temp_path) for temp_path in paths_to_mark
        ]
        expected_states = [Task("marker", "test"), Task("test", "done")]
        for temp_mark_filepath in marking_file_paths_which_should_exist:
            self.assertTrue(
                temp_mark_filepath.exists(),
                "The marker file '{}' should exist.".format(temp_mark_filepath),
            )
            self.assertTrue(state_of_tasks_are(temp_mark_filepath, expected_states))

    def test_mark_with_directory_option(self):
        remove_marker_files(self._temporary_dir)
        mark_with_name = "marker:test,test:done"
        paths_to_mark = [str(path) for path in self._temporary_test_paths]
        cli_arguments = [
            marktask.COMMAND_MARK_SHORT,
            marktask.COMMAND_MARK_OPTION_D,
            mark_with_name,
            *paths_to_mark,
        ]
        marktask.main(cli_arguments)
        marking_file_paths_which_should_exist = [
            marktask.propose_task_filepath(temp_path)
            for temp_path in paths_to_mark
            if Path(temp_path).is_dir()
        ]
        expected_states = [Task("marker", "test"), Task("test", "done")]
        for temp_mark_filepath in marking_file_paths_which_should_exist:
            self.assertTrue(
                temp_mark_filepath.exists(),
                "The marker file '{}' should exist.".format(temp_mark_filepath),
            )
            self.assertTrue(state_of_tasks_are(temp_mark_filepath, expected_states))

    def tearDown(self) -> None:
        myminions.remove_path_or_tree(self._temporary_dir)


class TestListing(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._temp_dir = None

    def setUp(self) -> None:
        self._temp_dir = Path(tempfile.mkdtemp())
        filepath_1 = self._temp_dir.joinpath("file-1.txt")
        filepath_2 = self._temp_dir.joinpath("file-2.txt")
        filepath_3 = self._temp_dir.joinpath("file-3.txt")
        self._filepaths = [filepath_1, filepath_2, filepath_3]
        for path in self._filepaths:
            path.touch()
        task1_done = Task("task1", "done")
        task1_prep = Task("task1", "prepared")
        task2_wait = Task("task2", "wait")
        marktask.mark_task(filepath_1, [task1_done, task2_wait])
        marktask.mark_task(filepath_2, [task1_prep, task2_wait])
        marktask.mark_task(filepath_3, [task1_prep, task2_wait])

    def test_list_task1_prepared(self):
        expected_file_paths = self._filepaths[1:]
        all_task1_prepared = list(
            marktask.iter_target_paths_with_task_states(
                root_paths=[self._temp_dir], requested_task_states=[Task("task1", "prepared")]
            )
        )
        self.assertEqual(expected_file_paths, all_task1_prepared)

    def test_list_task1_done(self):
        expected_file_paths = self._filepaths[:1]
        all_task1_prepared = list(
            marktask.iter_target_paths_with_task_states(
                root_paths=[self._temp_dir], requested_task_states=[Task("task1", "done")]
            )
        )
        self.assertEqual(expected_file_paths, all_task1_prepared)

    def tearDown(self) -> None:
        shutil.rmtree(self._temp_dir)


if __name__ == "__main__":
    unittest.main()
