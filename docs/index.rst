.. isisysvic3daccess documentation master file, created by
   sphinx-quickstart on Fri Sep 25 10:54:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=====================================
Welcome to marktask's documentation!
=====================================

Indices and tables
==================

* :ref:`genindex`

Installation
============

.. code-block:: shell

   $ pip install marktask


.. code-block:: shell

   $ pip install git+https://gitlab.com/david.scheliga/marktask.git@dev
