# Changelog
This changelog is inspired by [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [0.0a4] - 2020-10-15
### Changed
- using *get_piped_command_line_arguments* of myminions 

## [0.0a3] - 2020-10-14 - not released
### Added
- docstrings to (not all) methods
- doctest to filter methods
- unittest for listing marked files

### Fixed
- Severe bug within the filter process leading to wrong results.

## [0.0a2] - 2020-10-14 - not released
### Added
- changelog

### Fixed
- Wrongfully interpreted an empty string with the current working directory, if paths
  where supplied by the command line arguments.
- Old copied state of README and sphinx docs.

## [0.0a1] - 2020-10-03 - not released
Finished the first alpha state of the code.

### Added
- unittests_marktask.py
